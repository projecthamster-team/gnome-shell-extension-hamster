gnome-shell-extension-hamster (0.10.0+git20240714-2) unstable; urgency=medium

  [ Jeremy Bícha ]
  * Add patch to mark compatible with GNOME Shell 47 (Closes: #1079250)
  * Bump maximum GNOME Shell to 47

  [ Raphaël Hertzog ]
  * Bump Standards-Version to 4.7.0

 -- Raphaël Hertzog <hertzog@debian.org>  Sun, 22 Sep 2024 14:42:07 +0200

gnome-shell-extension-hamster (0.10.0+git20240714-1) unstable; urgency=medium

  * Acknowledge NMU. Reupload as it seems the NMU got lost in the delayed
    queue. (Closes: #1052101)

 -- Raphaël Hertzog <hertzog@debian.org>  Mon, 22 Jul 2024 21:24:12 +0200

gnome-shell-extension-hamster (0.10.0+git20240714-0.1) unstable; urgency=medium

  * Non-maintainer upload.
  * New upstream git snaphsot with GNOME Shell 46 compat. (Closes: #1052101)
  * Update version constraints on gnome-shell, as this version is only
    compatible with GNOME Shell 45 and up.

 -- Tobias Frost <tobi@debian.org>  Sun, 14 Jul 2024 09:43:35 +0200

gnome-shell-extension-hamster (0.10.0+git20230901-1) unstable; urgency=medium

  * New upstream git snapshot with GNOME Shell 44 compat. (Closes: #1041574)
  * Removes all patches.

 -- Raphaël Hertzog <hertzog@debian.org>  Fri, 01 Sep 2023 08:43:28 +0200

gnome-shell-extension-hamster (0.10.0+git20210628-4.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Update compatilbity patch for GNOME shell 44. (Closes: #1041574)

 -- Tobias Frost <tobi@debian.org>  Tue, 29 Aug 2023 10:58:19 +0200

gnome-shell-extension-hamster (0.10.0+git20210628-4) unstable; urgency=medium

  [ Jesús Soto ]
  * Add patch to mark compatible with GNOME Shell 43
  * debian/control: Bump maximum gnome-shell to 43 (Closes: #1018269)

  [ Raphaël Hertzog ]
  * Bump Standards-Version to 4.6.1

 -- Raphaël Hertzog <hertzog@debian.org>  Mon, 12 Sep 2022 11:49:25 +0200

gnome-shell-extension-hamster (0.10.0+git20210628-3) unstable; urgency=medium

  * Document compatibility with GNOME 42 (Closes: #1008549)

 -- Raphaël Hertzog <hertzog@debian.org>  Thu, 31 Mar 2022 22:49:01 +0200

gnome-shell-extension-hamster (0.10.0+git20210628-2) unstable; urgency=medium

  * Document compatibility with GNOME Shell 41.
    Tighten the gnome-shell dependency to not allow usage with gnome-shell 42.
    (Closes: #996060)

 -- Raphaël Hertzog <hertzog@debian.org>  Mon, 18 Oct 2021 15:15:36 +0200

gnome-shell-extension-hamster (0.10.0+git20210628-1) unstable; urgency=medium

  * Bump Standards-Version to 4.6.0
  * New upstream version 0.10.0+git20210628
    - Documents compatibility with GNOME 40. Closes: #993186

 -- Raphaël Hertzog <hertzog@debian.org>  Wed, 22 Sep 2021 10:32:08 +0200

gnome-shell-extension-hamster (0.10.0+git20200509-2) unstable; urgency=medium

  * No change source only upload.

 -- Raphaël Hertzog <hertzog@debian.org>  Wed, 03 Feb 2021 18:40:08 +0100

gnome-shell-extension-hamster (0.10.0+git20200509-1) unstable; urgency=medium

  * Initial release (Closes: #975974)

 -- Raphaël Hertzog <hertzog@debian.org>  Fri, 27 Nov 2020 14:45:40 +0100
